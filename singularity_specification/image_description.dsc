BootStrap: docker
From: ubuntu

%labels

%environment
export LC_ALL=C

%post
export DEBIAN_FRONTEND=noninteractive

apt-get update
apt-get install -y libhdf5-dev graphviz locales python3-dev python3-pip curl git
apt-get clean

pip3 install attrs==20.3.0
pip3 install certifi==2020.6.20
pip3 install chardet==4.0.0
pip3 install coverage==5.4
pip3 install cycler==0.10.0
pip3 install decorator==4.4.2
pip3 install idna==2.10
pip3 install imageio==2.5.0
pip3 install importlib-metadata==3.4.0
pip3 install iniconfig==1.1.1
pip3 install joblib==0.13.2
pip3 install kiwisolver==1.1.0
pip3 install lxml==4.6.2
pip3 install matplotlib==3.3.4
pip3 install mpmath==1.1.0
pip3 install networkx==2.5
pip3 install numpy==1.20.0
pip3 install packaging==20.9
pip3 install pandas==1.1.5
pip3 install Pillow==8.1.0
pip3 install pluggy==0.13.1
pip3 install git+git://github.com/titipata/pubmed_parser.git@ee0fce583cf111b71430660e743fd0d72acd68ed
pip3 install py==1.10.0
pip3 install pyparsing==2.4.7
pip3 install pytest==6.2.2
pip3 install pytest-cov==2.11.1
pip3 install python-dateutil==2.8.1
pip3 install pytz==2021.1
pip3 install PyWavelets==1.0.3
pip3 install requests==2.25.1
pip3 install git+https://github.com/SkBlaz/san@bb9aaea20687d04bfa29402db2adc41f1827f487
pip3 install scikit-image==0.15.0
pip3 install scikit-learn==0.24.1
pip3 install scipy==1.6.0
pip3 install seaborn==0.9.0
pip3 install shap==0.29.3
pip3 install six==1.15.0
pip3 install skope-rules==1.0.1
pip3 install sympy==1.4
pip3 install threadpoolctl==2.1.0
pip3 install toml==0.10.2
pip3 install torch==1.7.1
pip3 install tqdm==4.56.0
pip3 install typing-extensions==3.7.4.3
pip3 install Unidecode==1.1.2
pip3 install urllib3==1.26.3
pip3 install xgboost==0.90
pip3 install zipp==3.4.0

unset DEBIAN_FRONTEND