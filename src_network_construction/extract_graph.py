## this simple script extracts the global context graph
import networkx as nx
import numpy as np
import matplotlib.pyplot as plt
import tqdm

def read_links(flx):
    print("Extract graph ..")
    global_graph = {}
    with open(flx) as inf:
        for line in tqdm.tqdm(inf, total = 1000000000):
            parts = line.strip().split("\t")
            if len(parts) != 4:
                continue
            tag,n1,n2,w = parts
            if tag == "GRAPH_LINK":
                if (n1,n2) in global_graph:
                    global_graph[(n1,n2)] += int(w)
                else:
                    global_graph[(n1,n2)] = int(w)

    print("Num links: {}".format(len(global_graph)))
    print("Constructing the graph")
    G = nx.Graph()
    weights = []
    for k,v in global_graph.items():
        weights.append(int(v))
        n1,n2 = k
        w = int(v)
        G.add_edge(n1,n2,weight = w)
    print(nx.info(G))
    nx.write_gpickle(G,"../chemicals_graph/global_graph.gpickle")

def export_to_triplets(gpickle_graph):

    G = nx.read_gpickle(gpickle_graph)
    print(nx.info(G))
    all_rows = []
    
    for edge in G.edges(data=True):
        e1,e2,wx = edge
        weight = wx['weight']
        row = "\t".join([e1,e2,str(weight)])
        all_rows.append(row)
        
    with open("../chemicals_graph/raw_graph.tsv","w") as of:
        of.write("\n".join(all_rows))
        
if __name__ == "__main__":
    read_links("../chemicals_graph/main_graph.txt")
    export_to_triplets("../chemicals_graph/global_graph.gpickle")
