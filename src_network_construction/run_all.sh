
## download and parse pubmed.
#mkdir -p ../pubmed_xml;
#cd ../pubmed_xml;

# The updated files.
#wget -rv --no-parent --no-directories ftp://ftp.ncbi.nlm.nih.gov/pubmed/updatefiles > ../downloadout.log 2> ../downloaderr.log;

# Donwnload the baseline.
#wget -rv --no-parent --no-directories ftp://ftp.ncbi.nlm.nih.gov/pubmed/baseline > ../downloadout.log 2> ../downloaderr.log;

#rm *.md5;
#cd ../graph_construction
#echo "Done";

## generate a pubmed txt file.
mkdir ../chemicals_graph;
rm -rvf ../chemicals_graph/*;
ls ../pubmed_xml/*.gz| mawk '{print "python3 _pubmed.py --xml_file "$1}' | parallel --progress #> ../data_texts/pubmed_raw.txt

## todo -> extract chemicals graph.
echo "PMID extracts .."
cd ../chemicals_graph;cat main_graph_clean.txt | grep 'PMID' > pmid_extracts.tsv;
echo "Graph construction and filtering .."
cat main_graph_clean.txt | grep 'GRAPH' | mawk -F "\t" '{if ($4 > 1) {print $2"\t"$3"\t"$4}}' > raw_graph.tsv;
echo "Compression .."
gzip raw_graph.tsv;
gzip pmid_extracts.tsv;
mv *.gz ../data/
