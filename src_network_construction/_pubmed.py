## get abstracts
import tqdm
import logging
logging.basicConfig(format='%(asctime)s - %(message)s', datefmt='%d-%b-%y %H:%M:%S')
logging.getLogger().setLevel(logging.INFO)
from itertools import combinations

def parse_interactions(intifle):

    interactions = []
    with open(intifle) as inf:
        for line in inf:
            parts = line.strip().split()
            n1 = parts[7]
            n2 = parts[8]
            if len(n1) > 2 and len(n2) > 2 and n1 != n2:
                interactions.append((n1,n2))
    return interactions

def text_generator(textfiles):

    with open(textfiles) as tfx:
        for line in tfx:
            line = line.strip()
            yield line

if __name__ == "__main__":
    import glob
    import pubmed_parser as pp
    import argparse
    from collections import defaultdict
    
    parser = argparse.ArgumentParser()    
    parser.add_argument("--text_base",default="../data_texts/pubmed.txt", type = str)
    parser.add_argument("--annotation_type",default="chemicals", type = str)
    parser.add_argument("--interactions_file",default="../data_interactions/BIOGRID-MV-Physical-3.5.184.tab3.txt", type = str)
    parser.add_argument("--xml_file",default="../pubmed_xml/pubmed20n0007.xml.gz", type = str)
    
    args = parser.parse_args()
    weighted_links = {}
    pmid_to_mesh = defaultdict(set)
    
    try:
        dict_out = pp.parse_medline_xml(args.xml_file)
        
        for enx, paper in enumerate(dict_out):

            try:
                sentence = paper['abstract'].replace("\n","")
            except:
                sentence = paper['abstract']
            
            pid = paper['pmid']
            # if len(sentence.strip()) > 30:
            #     print(pid+"\t"+sentence.strip())
            #print(paper['mesh_terms'])
            
            if type(paper['mesh_terms']) == float or type(paper['mesh_terms']) == float:
                continue

            if args.annotation_type == "all":
                chemicals1 = paper['chemical_list'].split(";")
                chemicals2 = paper['mesh_terms'].split(";")
                
            else:
                chemicals1 = paper['chemical_list'].split(";")
                chemicals2 = [""]
                
            chemicals = list(set(chemicals1+chemicals2))
            chemicals = [x for x in chemicals if x != ""]
            
            if len(chemicals) == 0:
                continue
            
            for chemical in chemicals:
                pmid_to_mesh[pid].add(chemical)
            links = combinations(chemicals,2)            
            for link in links:
                link = tuple(link)
                if link in weighted_links:
                    weighted_links[link] += 1                    
                else:
                    weighted_links[link] = 1
                    
            if paper['delete']:
                continue
            
    except Exception as es:
        print(es,"invalid gzipped PubMed base.")

    links = []
    for k,v in pmid_to_mesh.items():
        vx = list(v)
        if len(vx) > 0:
            links.append("\t".join(["PMID_MESH",k,";".join(vx)]))
    output_mesh = "\n".join(links) + "\n"
        
    with open("../chemicals_graph/main_graph_clean.txt","a+") as cg:
        links = []
        for k,v in weighted_links.items():
            l1 = k[0].strip()
            l2 = k[1].strip()
            w = v
            links.append("\t".join(["GRAPH_LINK",l1,l2,str(w)]))
        output = "\n".join(links) + "\n" + output_mesh
        cg.write(output)
