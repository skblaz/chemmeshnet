## ChemMeSHNet a chemical entity graph for literature-based knowledge discovery
Welcome to ChemMeSHNet's repo. Here you can find the code to replicate and reconstruct the chemicals network,
which was precomputed and is available in the `data` folder.

## Requirements

To install the requirements, please perform the following command:
```
pip install -r requirements.txt
```
Note that Python >3.6 is needed for proper execution.

## Singularity image
A singularity container specification is available in the `singularity_specification` folder.

## Conda environment
The file `conda_env.yml` also contains the conda environment required to run the codebase. You can use it via:
```
conda env create -f conda_env.yml
```

## Network construction
from the folder `src_network_construction` run `run_all.sh` to create the MeSH term network.

## Learning
The representation learning part can be replicated by running the `run_all.sh` from the `src_network_reconstruction` folder.