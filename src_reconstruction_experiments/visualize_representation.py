import numpy as np
import matplotlib.pyplot as plt
import seaborn as sns
from sklearn.decomposition import PCA

nodelist = []
with open("../data/nodeNames.txt") as nn:
    for line in nn:
        entry = line.strip()
        nodelist.append(entry)

embedding_matrix = np.load("../data/node_embeddings.npy", allow_pickle=True)
embeddings = embedding_matrix.item().get('embeddings')
names = embedding_matrix.item().get('names')
labels = []
for n in names:
    if "protein, human" in n:
        labels.append("Human protein")
    else:
        labels.append("Other")
assert embeddings.shape[0] == len(names)
print("Embedding to 2d")
representation = PCA(n_components = 2).fit_transform(embeddings)

print("Plotting")
sns.scatterplot(representation[:,0],representation[:,1], hue = labels, style = labels,s = 8, palette = "coolwarm")
plt.tight_layout()
plt.savefig("../figures/projection.pdf", dpi = 300)
