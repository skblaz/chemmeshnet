## learning eval
import numpy as np
import pandas as pd
from sklearn.linear_model import LogisticRegression
import san
from sklearn.metrics import f1_score, roc_auc_score
from sklearn.model_selection import StratifiedShuffleSplit
from sklearn.ensemble import RandomForestClassifier
from sklearn.dummy import DummyClassifier
from scipy import sparse
import pickle
import six
import sys
sys.modules['sklearn.externals.six'] = six
from skrules import SkopeRules
import xgboost

def store_model(model, path):

    with open(path, 'wb') as output:
        pickle.dump(model, output, pickle.HIGHEST_PROTOCOL)


def evaluate_LR(X, Y, edim, names):

    shuffle = np.random.permutation(X.shape[0])
    X = X[shuffle]
    Y = Y[shuffle]
    names = [names[i] for i in shuffle]
    tsizes = [0.1]

    final_results = []
    learners = ["XGB","SkopeRules","SAN","RF","LR","Dummy"]
    for learner in learners:
        for tsize in tsizes:

            sss = StratifiedShuffleSplit(n_splits=10, test_size=tsize, random_state=0)
            for train_index, test_index in sss.split(X,Y):

                X_train = X[train_index]
                X_test = X[test_index]
                y_train = Y[train_index]
                y_test = Y[test_index]

                if learner == "RF":
                    clf = RandomForestClassifier(n_estimators = 100)
                    clf.fit(X_train,y_train)
                    preds = clf.predict(X_test)
                    
                elif learner == "LR":
                    clf = LogisticRegression(max_iter = 1000000)
                    clf.fit(X_train,y_train)
                    preds = clf.predict(X_test)

                elif learner == "autoML":
                    clf = AutoSklearn2Classifier()
                    clf.fit(X_train,y_train)
                    preds = clf.predict(X_test)

                elif learner == "XGB":
                    model = xgboost.XGBClassifier(n_estimators=200, max_depth=4, learning_rate=0.01, subsample=0.6, objective = "binary:logistic")
                    model.fit(X_train, y_train)
                    preds = model.predict(X_test)

                elif learner == "SpyCT":
                    clf = spyct.Model(balance_classes=True, splitter = "svm")
                    y_train = y_train.reshape(-1,1)
                    clf.fit(X_train,y_train)
                    X_test = np.asarray(X_test)
                    preds = clf.predict(X_test)
                    preds = np.argmax(preds, axis = 1)

                elif learner == "SkopeRules":
                    clf = SkopeRules()
                    clf.fit(X_train,y_train)
                    preds = clf.predict(X_test)

                elif learner == "SAN":
                    clf = san.SAN(num_epochs=30, num_heads=2, batch_size=1024, dropout=0.2, hidden_layer_size=64, stopping_crit = 10, learning_rate = 0.01)
                    clf.fit(X_train,y_train)
                    preds = clf.predict(X_test)

                elif learner == "Dummy":
                    clf = DummyClassifier()
                    clf.fit(X_train,y_train)
                    preds = clf.predict(X_test)
                try:
                    f1s = roc_auc_score(preds, y_test)
                except:
                    f1s = 0
                print(f"Testing size {tsize}, score {f1s}, learner: {learner}")
                final_results.append([learner,f1s,1-tsize])
        dfx = pd.DataFrame(final_results)

    print("Finished evaluation")
    dfx['dimension'] = edim
    dfx.columns = ['Learner','AUC','Train percentage','dimension']
    dfx.to_csv(f"../tables/prediction_results_{edim}.tsv",sep = "\t")

    final_model = san.SAN(num_epochs=300, num_heads=3, batch_size=1024, dropout=0.2, hidden_layer_size=64, stopping_crit = 10, learning_rate = 0.01)
    final_model.fit(X,Y)
    store_model(final_model, f"../models/san_{edim}.pickle")

    # for ablation of FP
    interestingFP = []
    preds = final_model.predict(X)
    enx = 0
    for el, real in zip(preds, Y):
        if real == 0 and el > 0.9:
            interestingFP.append([real, names[enx]])
        enx+=1
    dfx2 = pd.DataFrame(interestingFP)
    dfx2.columns = ['Probability','Interaction']
    dfx2.to_csv(f"../tables/falsepositives_{edim}.tsv",sep = "\t")
            
            
    
if __name__ == "__main__":


    for edim in [2,4,8,16,32,64,128,256,512]:
        embedding_matrix = np.load(f"../data/reconstruction_data_{edim}.npy", allow_pickle=True)
        embeddings = embedding_matrix.item().get('embeddings')
        target = embedding_matrix.item().get('target')
        names = embedding_matrix.item().get('all_links_training')
        print(embeddings.shape)
        print(target.shape)
        
        evaluate_LR(embeddings, target, edim, names)
