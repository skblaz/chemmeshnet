## spring layout

from py3plex.visualization.multilayer import hairball_plot, plt
import networkx as nx

G = nx.read_gpickle("../data/wholeNetwork.gpickle")
network_colors = ["black" if not "protein" in x else "green" for x in G.nodes()]

# visualize the network's communities!
hairball_plot(G,
              color_list=network_colors,
              layout_parameters={"iterations": 1},
              scale_by_size=False,
              edge_width=0.005,
              node_size = 0.1,
              alpha_channel=0.1,
              layout_algorithm="force",
              legend=False)

plt.show()
