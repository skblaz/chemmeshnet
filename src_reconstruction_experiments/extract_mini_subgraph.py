import networkx as nx
import gzip

G = nx.Graph()
with gzip.open("../data/raw_graph.tsv.gz", "rt") as gzo:
    for line in gzo:
        parts = line.strip().split("\t")
        n1,n2,w = parts
        G.add_edge(n1,n2, weight = w)

target_node = "C484806:CAPN1 protein, human"
tnodes = G.neighbors(target_node)
all_nodes = [target_node]
for node in tnodes:
    neighs = G.neighbors(node)
    for n in neighs:
        all_nodes.append(n)    
subgraph = G.subgraph(all_nodes)
for edge in subgraph.edges(data=True):
    if int(edge[2]['weight']) > 20:
        print("\t".join([edge[0], edge[1], edge[2]['weight']]))
