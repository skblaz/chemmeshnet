## prediction engine
import numpy as np

## train SAN on all training
embedding_matrix = np.load("../data/reconstruction_data.npy", allow_pickle=True)
embeddings = embedding_matrix.item().get('embeddings')
target = embedding_matrix.item().get('target')
clf = san.SAN(num_epochs=300, num_heads=3, batch_size=1024, dropout=0.2, hidden_layer_size=64, stopping_crit = 10, learning_rate = 0.01)

## fit the model
clf.fit(embeddings, target)

candidates = np.load("../data/candidate_interactions.npy", allow_pickle=True)
embeddings = embedding_matrix.item().get('embeddings')
target = embedding_matrix.item().get('all_links_training')
predictions = clf.predict_proba(embeddings)

