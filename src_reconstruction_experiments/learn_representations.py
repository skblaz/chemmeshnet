## a simple representation learning application of the SCD algorithm.

#import SCD
import scipy.io
import numpy as np
from scipy.sparse import csgraph,csr_matrix
from sklearn.decomposition import TruncatedSVD
random_seed = 4564324
graph = scipy.io.loadmat("../data/conceptGraph.mat")
graph = graph['network']

node_names = []
with open("../data/nodeNames.txt", "rt") as nn:
    for line in nn:
        part = line.strip()
        node_names.append(part)

for ndim in [2,4,8,16,32,64,128,256,512]:
    laplacian = csgraph.laplacian(graph, normed=True)
    print("Obtained the laplacian matrix")

    print("SVD")
    svd = TruncatedSVD(n_components=ndim, random_state=random_seed)
    node_embeddings = svd.fit_transform(laplacian)
    print(node_embeddings.shape)

    embd = {"embeddings":node_embeddings, "names": node_names}
    np.save(f"../data/node_embeddings_{ndim}.npy", embd)
