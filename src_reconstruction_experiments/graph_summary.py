import gzip
import networkx as nx
import matplotlib.pyplot as plt
import seaborn as sns
import numpy as np
import pandas as pd

def read_compressed_graph(tsv_file_gz):

    G = nx.Graph()
    weight_distribution = []
    cnter = 0
    with gzip.open(tsv_file_gz,"rt") as gzp:
        for line in gzp:
            parts = line.strip().split("\t")
            cnter += 1
            if len(parts) == 3:
                n1, n2, w = parts
                if int(w) > 1:
                    weight_distribution.append(int(w))
                    G.add_edge(n1,n2,weight = int(w))

    G.remove_edges_from(nx.selfloop_edges(G))
    print(nx.info(G))

    nodes = len(G.nodes())
    edges = len(G.edges())
    cc = len(list(nx.connected_components(G)))
    clustering = nx.average_clustering(G)
    dx = nx.density(G)
    mean_degree = np.mean([G.degree(j) for j in G.nodes()])
    rframe = pd.DataFrame()
    point = {"Name": "PubMedNet",
             "nodes":nodes,
             "edges":edges,
             "Mean degree":mean_degree,
             "connected components":cc,
             "clustering coefficient":clustering,
             "density":dx}
    
    rframe = rframe.append(point,ignore_index=True)
    print(point)
    rframe.index = rframe.columns    
    rframe.to_latex("../tables/summaryNet.tex", index = False)
    
    nx.write_gpickle(G, "../data/wholeNetwork.gpickle")
    wd = np.array(weight_distribution)
    logs = np.sort(wd)[::-1]
    plt.xlabel("Sorted indices")
    plt.ylabel("Degree")
    lrange = np.array(list(range(len(wd))))
    plt.yscale("log")
    plt.xscale("log")
    plt.plot(lrange, logs, color = "black")
    plt.savefig("../figures/degreeDist.pdf", dpi = 300)

    top_deg = sorted(G.degree, key=lambda x: x[1], reverse=True)
    top_deg_five = top_deg[0:5]
    print(top_deg_five)

    last_deg_five = top_deg[-5:]
    print(last_deg_five)
    
    return G


if __name__ == "__main__":

    import scipy.io as sio
    from scipy import sparse
    
    graph = read_compressed_graph("../data/raw_graph.tsv.gz")
    nodelist = list(graph.nodes())
    fx = open("../data/nodeNames.txt","w")
    fx.write("\n".join(nodelist))
    fx.close()
    mtx = nx.to_scipy_sparse_matrix(graph, format = "csr")
    out_dict = {"network": sparse.csr_matrix(mtx)}
    sio.savemat("../data/"+"conceptGraph.mat", out_dict)
