## read and parse the maing raw graph files
python graph_summary.py

## learn representations
python learn_representations.py

## visualize representations with UMAP
python visualize_representation.py

## compute reconstructions
python reconstruction_experiment.py

## learning experiments
python learning_evaluation.py

## novel candidates
python generate_candidates.py

## novel candidates ranked via SAN
python rank_with_san.py
