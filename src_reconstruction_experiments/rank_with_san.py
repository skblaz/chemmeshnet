## simple ranking
import pickle
import pandas as pd
import numpy as np
import tqdm

with open("../models/san.pickle", "rb") as sanModel:    
    trained_san = pickle.load(sanModel)

## load candidates
embedding_matrix = np.load("../data/candidate_interactions.npy", allow_pickle=True)
embeddings = embedding_matrix.item().get('embeddings')
names = embedding_matrix.item().get('all_links_training')

embedding_matrix2 = np.load("../data/reconstruction_data.npy", allow_pickle=True)
namesTrain = set(embedding_matrix2.item().get('all_links_training'))

assert embeddings.shape[0] == len(names)

output_df = []
for enx, name in tqdm.tqdm(enumerate(names), total = len(names)):
    if name in namesTrain:
        continue
    candidate_interaction = embeddings[enx]
    prediction = trained_san.predict_proba(candidate_interaction)
    row = [prediction, name]
    if not prediction == 1:
        output_df.append(row)

final_dfx = pd.DataFrame(output_df)
final_dfx.columns = ['probability','interaction']
final_dfx = final_dfx.sort_values(by = ['probability'], ascending = False)
final_dfx = final_dfx.iloc[0:100000]
final_dfx.to_csv("../tables/interactionsPredicted.tsv", sep = "\t")
