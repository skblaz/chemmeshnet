import numpy as np
import pandas as pd
import matplotlib.pyplot as plt
import seaborn as sns
sns.set_style('whitegrid')

dfx = pd.read_csv("../tables/OVERALL_DUMP2.tsv",sep = '\t')
dfx = dfx.sort_values(by = ['AUC'])

fig, ax = plt.subplots(1,1,figsize=[5,3])

ordering = ['Dummy','LR','SkopeRules','SAN','XGB','RF']
sns.barplot(dfx.Learner,
            dfx.AUC,
            hue = dfx.dimension,
            errwidth = 1,
            palette = "coolwarm",
            capsize = .1,
            ax=ax,
            zorder=5,
            order=ordering,
            ci=68) # SEM errorbars

plt.legend(prop={'size': 8})
plt.tight_layout()
plt.savefig("../figures/benchmark.pdf", dpi = 300)
