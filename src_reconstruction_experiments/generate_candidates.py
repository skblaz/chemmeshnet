## reconstruct empirical networks based on real ones.
import networkx as nx
from itertools import combinations
import numpy as np
import tqdm

def read_biogrid_graph(bgf_file):

    G = nx.Graph()
    with open(bgf_file) as bgf:
        for line in bgf:
            parts = line.strip().split("\t")
            name1 = [x for x in parts[2].split("|") if "locuslink:" in x]
            name2 = [x for x in parts[3].split("|") if "locuslink:" in x]
            name1 = " AND ".join(name1)
            name2 = " AND ".join(name2)
            detection = parts[6]
            pub = parts[8]
            itype = parts[11]
            G.add_edge(name1, name2, publication = pub, detection = detection)
    print(nx.info(G))
    return G

def get_representation_subgraph(biogrid_graph, embedding_space, names):

    present_nodes = {}
    for enx, name in tqdm.tqdm(enumerate(names), total = len(names)):
        if "protein, human" in name:
            for node in biogrid_graph.nodes():
                pname = name.split(" ")[0].split(":")[1]
                if pname in node:
                    present_nodes[node] = embedding_space[enx]
    
    print("Found representations for :",len(present_nodes),"nodes")
    subgraph_nodes = list(present_nodes.keys())
    repsub = biogrid_graph.subgraph(subgraph_nodes)
    return present_nodes, repsub

def generate_classification_problem(representations, subgraph, negative_ratio = 2):

    ## first, add all present edges
    final_representation = []
    final_targets = []
    all_links = set()
    all_considered_links = []
    for e in subgraph.edges():
        n1,n2 = e
        r1 = representations[n1]
        r2 = representations[n2]
        joint = np.concatenate([r1,r2]).reshape(-1)
        final_representation.append(joint)
        final_targets.append(1)
        all_links.add((n1,n2))
        all_considered_links.append((n1,n2))

    ## negative sampling
    all_nodes = list(subgraph.nodes())
    negative_counter = 0
    training_data_stored = True

    print("Negative sampling")    
    for n1, n2 in combinations(all_nodes, 2):

        if training_data_stored:
            if len(all_considered_links) > 1000000:
                final_data = np.matrix(final_representation)
                embd = {"embeddings":final_data,"all_links_training":all_considered_links}
                print("candidate interactions stored!")
                np.save("../data/candidate_interactions.npy", embd)
                break            
        
        if negative_counter > len(all_links) * negative_ratio:
            if training_data_stored:
                continue
            
            final_data = np.matrix(final_representation)
            final_targets = np.array(final_targets)
            assert final_data.shape[0] == len(final_targets)
            print(final_data.shape, final_targets.shape)
            embd = {"embeddings":final_data, "target": final_targets,"all_links_training":all_considered_links}
            np.save("../data/reconstruction_data.npy", embd)
            training_data_stored = True
            final_representation = []
            all_considered_links = []
            print("Stored reconstruction data")
        
        if not (n1,n2) in all_links and not (n2,n1) in all_links:
            if training_data_stored == False:
                try:
                    path_len = nx.shortest_path(subgraph, n1, n2, weight = "weight")
                except:
                    path_len = [1,2,3,4]
                if not len(path_len) > 2:
                    continue
            r1 = representations[n1]
            r2 = representations[n2]
            joint = np.concatenate([r1,r2]).reshape(-1)
            final_representation.append(joint)
            if not training_data_stored:
                final_targets.append(0)
            all_considered_links.append((n1,n2))
            negative_counter += 1
            

if __name__ == "__main__":

    ## read the biomine graph
    biogrid_path = "../biogrid_data/BIOGRID-MV-Physical-4.2.191.mitab.txt"
    biogrid_graph = read_biogrid_graph(biogrid_path)

    ## load the precomputed embeddings
    embedding_matrix = np.load("../data/node_embeddings.npy", allow_pickle=True)
    embeddings = embedding_matrix.item().get('embeddings')
    names = embedding_matrix.item().get('names')

    ## representation learning step
    representations_present, subgraph = get_representation_subgraph(biogrid_graph, embeddings, names)
    generate_classification_problem(representations_present, subgraph)
